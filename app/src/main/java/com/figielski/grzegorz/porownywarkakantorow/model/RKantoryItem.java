package com.figielski.grzegorz.porownywarkakantorow.model;

/**
 * Created by Grzesiek on 23.05.2017.
 */

public class RKantoryItem {

    private long id;
    private String nazwa;
    private String url;
    private String grafika;

    public RKantoryItem(long id, String nazwa, String url, String grafika){
        this.id =id;
        this.nazwa=nazwa;
        this.url=url;
        this.grafika=grafika;
    }

    public long getId() {
        return id;
    }

    public String getNazwa() {
        return nazwa;
    }

}
