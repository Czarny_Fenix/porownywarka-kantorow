package com.figielski.grzegorz.porownywarkakantorow;

import android.content.Context;
import android.util.Log;

import com.figielski.grzegorz.porownywarkakantorow.bazadanych.RemoteDataDbAdapter;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Grzesiek on 23.05.2017.
 */

public class RemoteDatabaseHandler {

    private static RemoteDatabaseHandler Instance = null;
    private static Context context;
    private final String TAG = "skurs";

    public static RemoteDatabaseHandler getInstance(Context context) {
        if(Instance == null){
            Instance = new RemoteDatabaseHandler(context);
        }
        return Instance;
    }

    RemoteDatabaseHandler(Context context){
        this.context = context;
    }

    /**
     *
     * metoda zwracajaca wszystkie kantory z bazy zewnetrznej w postaci stringa w formacie JSON
     *
     */

    private String getKantoryString(){
        String kantory = "";
        try {
            URL url = new URL(context.getResources().getString(R.string.walutex_kantory_url));
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            InputStream in = new BufferedInputStream(conn.getInputStream());
            kantory = przepiszStream(in);
        } catch (Exception e){
            Log.e(context.getResources().getString(R.string.zlapano_exception), context.getResources().getString(R.string.remotedatabasehandler));
        }
        return kantory;
    }

    /**
     *
     * @param data              - data z ktorej ma zostac zczytany kurs
     * @param waluta            - waluta dla ktorej ma zostac zczytany kurs
     * @param idKantoru         - identyfikator kantoru w zewnetrznej bazie dla ktorego ma zostac zczytany kurs
     *
     * metoda zwracajaca kurs w postaci Stringa w formacie JSON z bazy zewnetrznej dla podanych parametrow
     *
     */

    private String getKursString(String data, String waluta, String idKantoru){
        String kurs = "";
        try {
            URL url = new URL(context.getResources().getString(R.string.walutex_waluta_url) + waluta +
                    "&kantor=" + idKantoru + "&date=" + data);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            InputStream in = new BufferedInputStream(conn.getInputStream());
            kurs = przepiszStream(in);
        } catch (Exception e){
            Log.e(context.getResources().getString(R.string.zlapano_exception), context.getResources().getString(R.string.remotedatabasehandler));
        }
        return kurs;
    }

    /**
     *
     * metoda przepisujaca podany InputStream do Stringa
     *
     */

    private String przepiszStream(InputStream stream){
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        StringBuilder pelna_tabela = new StringBuilder();
        String linia;
        try{
            while ((linia = reader.readLine()) != null) {
                pelna_tabela.append(linia);
            }
        } catch (Exception e){
            Log.e(context.getResources().getString(R.string.zlapano_exception), context.getResources().getString(R.string.remotedatabasehandler));
        }
        return new String(pelna_tabela);
    }

    /**
     *
     * @param data_zapytania        - data, z ktorej maja byc pobrane kursy
     *
     * metoda pobierajaca kursy dla podanej daty dla wszystkich kantorow znajdujacych sie w zewnetrznej bazie, aby
     * nastepnie nadpisac ostatnio pobierane kursy w lokalnym fragmencie bazy zewnetrznej
     *
     */

    public void kursyRemote(String data_zapytania){
        String kantoryString = getKantoryString();
        RemoteDataDbAdapter remotekursy = new RemoteDataDbAdapter(context);
        String id;
        String nazwa;
        int grafika = 0;                                            //grafika dla danego kantoru, wczytywana z lokalnych zasobow
        remotekursy.open();
        int iloscRekordow = remotekursy.getIloscRekordow();
        try {
            JSONArray kantory_d = new JSONArray(kantoryString);
            for (int i = 0; i < kantory_d.length(); i++) {
                JSONObject c = kantory_d.getJSONObject(i);

                id = c.getString("ID");
                nazwa = c.getString("NAZWA");

                String kurs_eur = getKursString(data_zapytania ,context.getResources().getString(R.string.eur), id);
                JSONArray tabela_eur = new JSONArray(kurs_eur);
                JSONObject eur = tabela_eur.getJSONObject(0);
                JSONObject eur2 = tabela_eur.getJSONObject(1);
                String kurs_usd = getKursString(data_zapytania ,context.getResources().getString(R.string.usd), id);
                JSONArray tabela_usd = new JSONArray(kurs_usd);
                JSONObject usd = tabela_usd.getJSONObject(0);
                JSONObject usd2 = tabela_usd.getJSONObject(1);
                String kurs_gbp = getKursString(data_zapytania ,context.getResources().getString(R.string.gbp), id);
                JSONArray tabela_gbp = new JSONArray(kurs_gbp);
                JSONObject gbp = tabela_gbp.getJSONObject(0);
                JSONObject gbp2 = tabela_gbp.getJSONObject(1);
                String kurs_chf = getKursString(data_zapytania ,context.getResources().getString(R.string.chf), id);
                JSONArray tabela_chf = new JSONArray(kurs_chf);
                JSONObject chf = tabela_chf.getJSONObject(0);
                JSONObject chf2 = tabela_chf.getJSONObject(1);

                if(nazwa.toLowerCase().contains(context.getResources().getString(R.string.nbp).toLowerCase())){
                    grafika = R.raw.kantor_logo_nbp;
                } else if(nazwa.toLowerCase().contains(context.getResources().getString(R.string.search_walutomat))){
                    grafika = R.raw.kantor_logo_walutomat;
                } else if(nazwa.toLowerCase().contains(context.getResources().getString(R.string.search_cinkciarz))){
                    grafika = R.raw.kantor_logo_cinkciarz;
                } else if(nazwa.toLowerCase().contains(context.getResources().getString(R.string.search_alior))){
                    grafika = R.raw.kantor_logo_alior;
                } else if(nazwa.toLowerCase().contains(context.getResources().getString(R.string.search_internetowy))){
                    grafika = R.raw.kantor_logo_internetowy;
                } else if(nazwa.toLowerCase().contains(context.getResources().getString(R.string.search_eksper))){
                    grafika = R.raw.kantor_logo_ekspert;
                }

                if(i+1 <= iloscRekordow) {
                    remotekursy.updateRemoteKursy(i+1, nazwa, Float.parseFloat(eur.getString(TAG)), Float.parseFloat(eur2.getString(TAG)),
                            Float.parseFloat(usd.getString(TAG)), Float.parseFloat(usd2.getString(TAG)),
                            Float.parseFloat(gbp.getString(TAG)),Float.parseFloat(gbp2.getString(TAG)),
                            Float.parseFloat(chf.getString(TAG)),Float.parseFloat(chf2.getString(TAG)), grafika);
                } else {
                    remotekursy.insertRemoteKursy(nazwa, Float.parseFloat(eur.getString(TAG)), Float.parseFloat(eur2.getString(TAG)),
                            Float.parseFloat(usd.getString(TAG)), Float.parseFloat(usd2.getString(TAG)),
                            Float.parseFloat(gbp.getString(TAG)),Float.parseFloat(gbp2.getString(TAG)),
                            Float.parseFloat(chf.getString(TAG)),Float.parseFloat(chf2.getString(TAG)), grafika);
                }

            }
            remotekursy.close();
        } catch (Exception e){
            Log.e(context.getResources().getString(R.string.zlapano_exception), context.getResources().getString(R.string.remotedatabasehandler));
            remotekursy.close();
        }
    }
}
