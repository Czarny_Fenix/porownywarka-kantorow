package com.figielski.grzegorz.porownywarkakantorow.fragmenty;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.figielski.grzegorz.porownywarkakantorow.R;
import com.figielski.grzegorz.porownywarkakantorow.adapter.DialogListAdapter;
import com.figielski.grzegorz.porownywarkakantorow.model.DialogListRow;


/**
 * Created by Grzesiek on 23.05.2017.
 */

public class ListaDialogFragment extends DialogFragment {
    private ListView lista;
    private Context context;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View rootView = getActivity().getLayoutInflater().inflate(R.layout.dialog_list_layout, null);

        context = rootView.getContext();
        lista = (ListView) rootView.findViewById(R.id.lista_dat_wykresu);
        final DialogListRow lista_data[]= new DialogListRow[5];
        lista_data[0]= new DialogListRow(context.getResources().getString(R.string.ostatnie_dni_trzy), 3);
        lista_data[1]= new DialogListRow(context.getResources().getString(R.string.ostatnie_dni_tydzien), 7);
        lista_data[2]= new DialogListRow(context.getResources().getString(R.string.ostatnie_dni_miesiac), 30);
        lista_data[3]= new DialogListRow(context.getResources().getString(R.string.ostatnie_dni_kwartal), 90);
        lista_data[4]= new DialogListRow(context.getResources().getString(R.string.ostatnie_dni_rok), 365);

        final DialogListAdapter adapter = new DialogListAdapter(rootView.getContext(), R.layout.dialog_list_row, lista_data);
        lista.setAdapter(adapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int war = lista_data[position].getWartosc();
                String nazwa = lista_data[position].getNazwa();
                GraphFragment fragmentMatka = (GraphFragment) getTargetFragment();
                fragmentMatka.pobierzWykres(war, nazwa);
                dismiss();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(context.getResources().getString(R.string.pobierz_wykres_dla)).setView(rootView);

        return builder.create();
    }
}
