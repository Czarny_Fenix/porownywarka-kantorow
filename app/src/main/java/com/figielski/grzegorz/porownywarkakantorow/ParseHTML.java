package com.figielski.grzegorz.porownywarkakantorow;

import android.content.Context;
import android.util.Log;

import com.figielski.grzegorz.porownywarkakantorow.model.Waluta;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.net.URL;
import java.util.regex.Pattern;

import static java.lang.Float.parseFloat;

/**
 * Created by Grzesiek on 30.04.2017.
 */

public class ParseHTML {

    private static ParseHTML Instance = null;
    private static Context context;

    static ParseHTML getInstance(Context context) {
        if(Instance == null){
            Instance = new ParseHTML(context);
        }
        return Instance;
    }

    ParseHTML(Context context){
        this.context = context;
    }

    /**
     *
     * ponizej znajduja sie deklaracje zmiennych uzywanych przez algorytm pobierania i algorytm
     * scrapowania danych
     *
     */

    private boolean blad;
    private String url;
    private float kursEur;
    private float kursUsd;
    private float kursChf;
    private float kursGbp;
    private float kursEur2;
    private float kursUsd2;
    private float kursChf2;
    private float kursGbp2;
    private static short flagaEur=0;
    private static short flagaUsd=1;
    private static short flagaChf=2;
    private static short flagaGbp=3;
    private static short euro=0;
    private static short dolar=1;
    private static short frank=2;
    private static short funt=3;

    private void setBlad(boolean blad) {
        this.blad = blad;
    }

    public boolean isBlad() {
        return blad;
    }

    private float getKursEur() {
        return kursEur;
    }

    private float getKursUsd() {
        return kursUsd;
    }

    private float getKursChf() {
        return kursChf;
    }

    private float getKursGbp() {
        return kursGbp;
    }

    private float getKursEur2() {
        return kursEur2;
    }

    private float getKursUsd2() {
        return kursUsd2;
    }

    private float getKursChf2() {
        return kursChf2;
    }

    private float getKursGbp2() {
        return kursGbp2;
    }

    public Waluta getEur() {
        return new Waluta(getKursEur(), getKursEur2());
    }

    public Waluta getUsd() {
        return new Waluta(getKursUsd(), getKursUsd2());
    }

    public Waluta getChf() {
        return new Waluta(getKursChf(), getKursChf2());
    }

    public Waluta getGbp() {
        return new Waluta(getKursGbp(), getKursGbp2());
    }

    /**
     *
     * @param url       - url zrodla
     *
     * selektor dla metody scrapujacej kursy ze stron www, dobiera on zestaw parametrow dla algorytmu
     * aby mogl on obsluzyc podany url zrodla poprawnie
     *
     * pierwsza flaga oznacza, ze zrodlo dostarcza tylko kurs sredni, natomiast druga flaga oznacza
     * ze potrzebne jest jeszcze drugie wyrazenie regularne, ktore zawezi wyniki selektora elementow
     *
     * tmp to tabela zawierajaca wyszukiwalne w zawartosci danej strony nazwy walut
     */

    public void waluty(String url){

        cleaner();
        setBlad(false);

        if(url.contains(context.getResources().getString(R.string.search_walutomat))){
            String[] tmp = context.getResources().getStringArray(R.array.znaczniki_walutomat);
            String regex = context.getResources().getString(R.string.regex_walutomat);
            boolean[] flagi = {true, false};
            walutyZunifikowane(url, regex, true, tmp, flagi, null);
        } else if(url.contains(context.getResources().getString(R.string.search_cinkciarz))){
            String[] tmp = context.getResources().getStringArray(R.array.znaczniki_cinkciarz);
            String regex = context.getResources().getString(R.string.regex_cinkciarz);
            walutyZunifikowane(url, regex, false, tmp, null, null);
        } else if(url.contains(context.getResources().getString(R.string.search_alior))){
            String[] tmp = context.getResources().getStringArray(R.array.znaczniki_default);
            String regex = context.getResources().getString(R.string.regex_alior);
            walutyZunifikowane(url, regex, false, tmp, null, null);
        } else if(url.contains(context.getResources().getString(R.string.search_internetowy))){
            String[] tmp = context.getResources().getStringArray(R.array.znaczniki_default);
            String regex = context.getResources().getString(R.string.regex_internetowy);
            String regex2 = context.getResources().getString(R.string.regex_internetowy2);
            boolean[] flagi = {false, true};
            walutyZunifikowane(url, regex, false, tmp, flagi, regex2);
        } else if(url.contains(context.getResources().getString(R.string.search_eksper))){
            String[] tmp = context.getResources().getStringArray(R.array.znaczniki_default);
            String regex = context.getResources().getString(R.string.regex_ekspert);
            walutyZunifikowane(url, regex, true, tmp, null, null);
        } else if(url.contains(context.getResources().getString(R.string.search_nbp))){
            URL url2;
            try{
                url2=new URL(url);
                ParseXML.getInstance(context).walutyNBP(url2);
                this.kursEur=ParseXML.getInstance(context).getKursEur();
                this.kursEur2=ParseXML.getInstance(context).getKursEur();
                this.kursUsd=ParseXML.getInstance(context).getKursUsd();
                this.kursUsd2=ParseXML.getInstance(context).getKursUsd();
                this.kursChf=ParseXML.getInstance(context).getKursChf();
                this.kursChf2=ParseXML.getInstance(context).getKursChf();
                this.kursGbp=ParseXML.getInstance(context).getKursGbp();
                this.kursGbp2=ParseXML.getInstance(context).getKursGbp();
            } catch(Exception e){
                Log.e(context.getResources().getString(R.string.zlapano_exception), context.getResources().getString(R.string.parsehtml));
                setBlad(true);
            }
        }
    }

    /**
     *
     * @param url           - url zrodla
     * @param regEx         - wyrazenie regularne sluzace do zawezenia interesujacego kodu zrodla
     * @param znak          - znak dzielacy wartosc kursu na stronie (przecinek, lub kropka)
     * @param znaczniki     - stringi jakie wystepuja w kodzie zrodla opisujace kursy (np. EUR/PLN ,lub Euro)
     * @param flagi         - flagi pomocnicze (opisane przy selektorze)
     * @param regEx2        - drugie wyrazenie regularne, do jeszcze wiekszego zawezenia interesujacego kodu zrodla
     */

    private void walutyZunifikowane(String url, String regEx, boolean znak, String[] znaczniki,
                                   boolean[] flagi, String regEx2) {

        boolean[] flagiPrzerwania = {false, false, false, false};           //flagi kontrolujace kiedy przerwac algorytm
        this.url = url;
        try {
            Document doc = Jsoup.connect(url).get();
            Elements kursyfield = doc.select(regEx);
            String pole;                                                    //pojedyncza klasa/pole wycieta z htmla zrodla
            String dzielnik;                                                //znak do dzielenia stringow w miejscu kursu

            if(znak){
                dzielnik = context.getResources().getString(R.string.kropka);
            } else{
                dzielnik = context.getResources().getString(R.string.przecinek);
            }

            Waluta waluta;
            for (int i=0; i<kursyfield.size(); i++) {
                pole = kursyfield.get(i).toString();
                if (pole.contains(znaczniki[euro])) {
                    waluta=scrapper(kursyfield, pole, dzielnik, i, flagi, regEx2);
                    this.kursEur=waluta.getK();
                    this.kursEur2=waluta.getS();
                    flagiPrzerwania[flagaEur] = true;
                } else if (pole.contains(znaczniki[dolar])) {
                    waluta=scrapper(kursyfield, pole, dzielnik, i, flagi, regEx2);
                    this.kursUsd=waluta.getK();
                    this.kursUsd2=waluta.getS();
                    flagiPrzerwania[flagaUsd] = true;
                } else if (pole.contains(znaczniki[frank])) {
                    waluta=scrapper(kursyfield, pole, dzielnik, i, flagi, regEx2);
                    this.kursChf=waluta.getK();
                    this.kursChf2=waluta.getS();
                    flagiPrzerwania[flagaChf] = true;
                } else if (pole.contains(znaczniki[funt])) {
                    waluta=scrapper(kursyfield, pole, dzielnik, i, flagi, regEx2);
                    this.kursGbp=waluta.getK();
                    this.kursGbp2=waluta.getS();
                    flagiPrzerwania[flagaGbp] = true;
                }
                if (flagiPrzerwania[flagaEur] && flagiPrzerwania[flagaUsd] &&
                        flagiPrzerwania[flagaChf] && flagiPrzerwania[flagaGbp]) {
                    break;
                }
            }
        } catch (Exception e) {
            Log.e(context.getResources().getString(R.string.zlapano_exception), context.getResources().getString(R.string.parsehtml));
            setBlad(true);
        }
    }

    /**
     *
     * @param kursyfield    - zawartosc dokumentu po zastosowaniu pierwszego wyrazenia regularnego (parametr uzywany
     *                        tylko w przypadku koniecznosci uzycia drugiego wyrazenia regularnego)
     * @param pole          - pole zawierajace interesujace nas dane
     * @param dzielnik      - znak dzielacy kurs, uzywany do splitu stringa (przecinek albo kropka)
     * @param i             - iteracja pentli w ktorej scrapper zostal wywolany (uzywana w przypadku uzycia drugiego regExa)
     * @param flagi         - flagi pomocnicze opisane przy selektorze
     * @param regEx2        - drugie wyrazenie regularne, do jeszcze wiekszego zawezenia interesujacego kodu zrodla
     * @return
     *
     * metoda majaca za zadanie wyciagnac kursy oidabeh waluty z pola i zwrocic je w formie modelu Waluta
     *
     */

    private Waluta scrapper(Elements kursyfield, String pole, String dzielnik, int i, boolean[] flagi, String regEx2){
        String kursX;                                                   //kurs kupna
        String kursY;                                                   //kurs sprzedazy
        String[] tmp;                                                   //tablica podzielonego pola przez dzielnik
        String iceDrugiRegEx;                                           //tablica przechowujaca pole uzyskane
                                                                        //przy pomocy drugiego regExa
        int j = 0;                                                      //zmienna pomagajaca znalezc kursy w podzielonym
                                                                        //polu (w tmp po splicie)

        Pattern pattern = Pattern.compile(context.getResources().getString(R.string.pattern_cyfra));
                                                                        //pattern sluzacy do wyszukiwania cyfr w
                                                                        //podzielonym polu (tmp)

        if(flagi==null || !flagi[1]){
            tmp = pole.split(dzielnik);
        } else{
            iceDrugiRegEx = kursyfield.get(i).select(regEx2).get(1).toString();
            tmp = iceDrugiRegEx.split(dzielnik);
        }
        while(!pattern.matcher(tmp[j].substring(tmp[j].length() - 1)).matches()){
            j++;
        }
        kursX = tmp[j].substring(tmp[j].length() - 1) + "." + tmp[j+1].substring(0, 4);
        j++;
        if(flagi==null || !flagi[0]){
            if(flagi!=null && flagi[1]){
                iceDrugiRegEx = kursyfield.get(i).select(regEx2).get(2).toString();
                tmp = iceDrugiRegEx.split(dzielnik);
                j--;
            }
            while(!pattern.matcher(tmp[j].substring(tmp[j].length() - 1)).matches()){
                j++;
            }
            kursY = tmp[j].substring(tmp[j].length() - 1) + "." + tmp[j+1].substring(0, 4);
            return new Waluta(parseFloat(kursX), parseFloat(kursY));
        }
        return new Waluta(parseFloat(kursX), parseFloat(kursX));
    }

    /**
     *
     * metoda uzywana do wyczyszczenia wartosci kursow w klasie
     *
     */

    private void cleaner(){
        this.kursEur    = -1;
        this.kursEur2   = -1;
        this.kursUsd    = -1;
        this.kursUsd2   = -1;
        this.kursChf    = -1;
        this.kursChf2   = -1;
        this.kursGbp    = -1;
        this.kursGbp2   = -1;
    }
}
