package com.figielski.grzegorz.porownywarkakantorow.model;

import java.sql.Timestamp;

/**
 * Created by Grzesiek on 29.03.2017.
 */

public class KursyItem {
    private long id;
    private long idk;
    private String nazwa;
    private float wartosck;
    private float wartoscs;
    private long czas;


    public KursyItem(long idk, String nazwa, Waluta waluta, long time){
        this.idk = idk;
        this.nazwa = nazwa;
        this.wartosck = waluta.getK();
        this.wartoscs = waluta.getS();
        this.czas = time;
    }

    public long getId() {
        return id;
    }

    public long getIdk() {
        return idk;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public float getWartosck() {
        return wartosck;
    }

    public float getWartoscs() {
        return wartoscs;
    }

    public long getTimestamp(){
        return czas;
    }

}
