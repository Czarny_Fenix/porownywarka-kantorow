package com.figielski.grzegorz.porownywarkakantorow.bazadanych;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.figielski.grzegorz.porownywarkakantorow.model.KursyRow;

/**
 * Created by Grzesiek on 28.05.2017.
 */

public class RemoteDataDbAdapter {
    private SQLiteDatabase db;
    private Context context;
    private RemoteDataDbAdapter.DatabaseHelper dbHelper;

    public RemoteDataDbAdapter(Context context) {                                    //konstruktor
        this.context = context;
    }

    /**
     *
     * ponizej stale uzywane do konstrukcji i poruszania sie po bazie danych
     *
     */

    private static final String DEBUG_TAG = "SqLiteTodoManager";
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "kursyremote.db";
    private static final String DB_KURSYREMOTE_TABLE = "kursyremote";

    private static final String KEY_ID = "id";                                       //kolumna ID
    private static final String ID_OPTIONS = "INTEGER PRIMARY KEY AUTOINCREMENT";
    private static final int ID_COLUMN = 0;
    private static final String KEY_N_KANTOR = "n_kantor";                           //kolumna nazwy kantoru
    private static final String N_KANTOR_OPTIONS = "STRING NOT NULL";
    private static final int N_KANTOR_COLUMN = 1;
    private static final String KEY_EUR_K = "eur_k";                                 //kolumna kupna euro
    private static final String EUR_K_OPTIONS = "FLOAT NOT NULL";
    private static final int EUR_K_COLUMN = 2;
    private static final String KEY_EUR_S = "eur_s";                                 //kolumna sprzedazy euro
    private static final String EUR_S_OPTIONS = "FLOAT NOT NULL";
    private static final int EUR_S_COLUMN = 3;
    private static final String KEY_USD_K = "usd_k";                                 //kolumna kupna euro
    private static final String USD_K_OPTIONS = "FLOAT NOT NULL";
    private static final int USD_K_COLUMN = 4;
    private static final String KEY_USD_S = "usd_s";                                 //kolumna sprzedazy euro
    private static final String USD_S_OPTIONS = "FLOAT NOT NULL";
    private static final int USD_S_COLUMN = 5;
    private static final String KEY_GBP_K = "gbp_k";                                 //kolumna kupna euro
    private static final String GBP_K_OPTIONS = "FLOAT NOT NULL";
    private static final int GBP_K_COLUMN = 6;
    private static final String KEY_GBP_S = "gbp_s";                                 //kolumna sprzedazy euro
    private static final String GBP_S_OPTIONS = "FLOAT NOT NULL";
    private static final int GBP_S_COLUMN = 7;
    private static final String KEY_CHF_K = "chf_k";                                 //kolumna kupna euro
    private static final String CHF_K_OPTIONS = "FLOAT NOT NULL";
    private static final int CHF_K_COLUMN = 8;
    private static final String KEY_CHF_S = "chf_s";                                 //kolumna sprzedazy euro
    private static final String CHF_S_OPTIONS = "FLOAT NOT NULL";
    private static final int CHF_S_COLUMN = 9;
    private static final String KEY_GRAFIKA = "grafika";                             //kolumna grafiki
    private static final String GRAFIKA_OPTIONS = "INTEGER";
    private static final int GRAFIKA_COLUMN = 10;


    private static final String DB_CREATE_KURSYREMOTE_TABLE =                       //stworzenie tabeli
            "CREATE TABLE " + DB_KURSYREMOTE_TABLE + "( " +
                    KEY_ID + " " + ID_OPTIONS + ", " +
                    KEY_N_KANTOR + " " + N_KANTOR_OPTIONS + ", " +
                    KEY_EUR_K + " " + EUR_K_OPTIONS + ", " +
                    KEY_EUR_S + " " + EUR_S_OPTIONS + ", " +
                    KEY_USD_K + " " + USD_K_OPTIONS + ", " +
                    KEY_USD_S + " " + USD_S_OPTIONS + ", " +
                    KEY_GBP_K + " " + GBP_K_OPTIONS + ", " +
                    KEY_GBP_S + " " + GBP_S_OPTIONS + ", " +
                    KEY_CHF_K + " " + CHF_K_OPTIONS + ", " +
                    KEY_CHF_S + " " + CHF_S_OPTIONS + ", " +
                    KEY_GRAFIKA + " " + GRAFIKA_OPTIONS +
                    ");";
    private static final String DROP_KURSYREMOTE_TABLE =                                      //usuniecie tabeli
            "DROP TABLE IF EXISTS " + DB_KURSYREMOTE_TABLE;


    /**
     *
     * klasa uzywana do tworzenia i aktualizowania bazy danych
     *
     */

    private static class DatabaseHelper extends SQLiteOpenHelper {
        private DatabaseHelper(Context context, String name,
                              SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        /**
         *
         * metoda uruchamiana przy tworzeniu bazy danych
         *
         */

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE_KURSYREMOTE_TABLE);

            Log.d(DEBUG_TAG, "Database creating...");
            Log.d(DEBUG_TAG, "Table " + DB_KURSYREMOTE_TABLE + " ver." + DB_VERSION + " created");

        }

        /**
         *
         * metoda uruchamiana, gdy po aktualizacji aplikacji
         * baza danych ma wyzsza wersje niz przed aktualizacja
         *
         */

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(DROP_KURSYREMOTE_TABLE);

            Log.d(DEBUG_TAG, "Database updating...");
            Log.d(DEBUG_TAG, "Table " + DB_KURSYREMOTE_TABLE + " updated from ver." + oldVersion + " to ver." + newVersion);
            Log.d(DEBUG_TAG, "All data is lost.");

            onCreate(db);
        }
    }

    //////////////////////////////////////// FUNKCJE ////////////////////////////////////////

    /**
     *
     * metoda uzywana do otwarcia dostepu do bazy danych
     *
     */

    public RemoteDataDbAdapter open(){
        dbHelper = new RemoteDataDbAdapter.DatabaseHelper(context, DB_NAME, null, DB_VERSION);
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLException e) {
            db = dbHelper.getReadableDatabase();
        }
        return this;
    }

    /**
     *
     * metoda uzywana do zamkniecia dostepu do bazy danych
     *
     */

    public void close() {                                                       //zamkniecie bazy
        dbHelper.close();
    }

    public static String getDbRemoteKursyTable() {
        return DB_KURSYREMOTE_TABLE;
    }

    /**
     *
     * metoda dodajaca nowe kursy do bazy danych
     *
     */

    public long insertRemoteKursy(String nazwa, float eurk, float eurs, float usdk, float usds,
                                  float gbpk, float gbps, float chfk, float chfs, int grafika){
        ContentValues newValues = new ContentValues();
        newValues.put(KEY_N_KANTOR, nazwa);
        newValues.put(KEY_EUR_K, eurk);
        newValues.put(KEY_EUR_S, eurs);
        newValues.put(KEY_USD_K, usdk);
        newValues.put(KEY_USD_S, usds);
        newValues.put(KEY_GBP_K, gbpk);
        newValues.put(KEY_GBP_S, gbps);
        newValues.put(KEY_CHF_K, chfk);
        newValues.put(KEY_CHF_S, chfs);
        newValues.put(KEY_GRAFIKA, grafika);
        return db.insert(getDbRemoteKursyTable(), null, newValues);
    }

    /**
     *
     * metoda aktualizujaca istniejace kursy w bazie danych
     *
     */

    public boolean updateRemoteKursy(long id, String nazwa, float eurk, float eurs, float usdk, float usds,
                                     float gbpk, float gbps, float chfk, float chfs, int grafika){

        String where = KEY_ID + "=" + id;

        ContentValues updateRemoteWalutyValues = new ContentValues();
        updateRemoteWalutyValues.put(KEY_N_KANTOR, nazwa);
        updateRemoteWalutyValues.put(KEY_EUR_K, eurk);
        updateRemoteWalutyValues.put(KEY_EUR_S, eurs);
        updateRemoteWalutyValues.put(KEY_USD_K, usdk);
        updateRemoteWalutyValues.put(KEY_USD_S, usds);
        updateRemoteWalutyValues.put(KEY_GBP_K, gbpk);
        updateRemoteWalutyValues.put(KEY_GBP_S, gbps);
        updateRemoteWalutyValues.put(KEY_CHF_K, chfk);
        updateRemoteWalutyValues.put(KEY_CHF_S, chfs);
        updateRemoteWalutyValues.put(KEY_GRAFIKA, grafika);                     //update danych
        return db.update(getDbRemoteKursyTable(), updateRemoteWalutyValues, where, null) > 0;
    }

    /**
     *
     * metoda zwracajaca ilosc rekordow w tabeli
     *
     */

    public int getIloscRekordow(){
        int ilosc;
        Cursor cur = db.rawQuery("SELECT COUNT(*) FROM kursyremote", null);
        cur.moveToFirst();
        ilosc = cur.getInt(0);
        return  ilosc;
    }

    /**
     *
     * @param id
     *
     * metoda zwracajaca rekord w postaci KursyRow o podanym id
     *
     */

    public KursyRow getRowAtPosiotion(long id){
            String[] columns = {KEY_ID, KEY_N_KANTOR, KEY_EUR_K, KEY_EUR_S, KEY_USD_K, KEY_USD_S, KEY_GBP_K, KEY_GBP_S,
            KEY_CHF_K, KEY_CHF_S, KEY_GRAFIKA};
            String where = KEY_ID + "=" + Long.toString(id);
            Cursor cursor = db.query(getDbRemoteKursyTable(), columns, where, null, null, null, null);
            KursyRow waluta = null;
            if(cursor != null && cursor.moveToFirst()) {

                String nazwa = cursor.getString(N_KANTOR_COLUMN);
                float eur_k = cursor.getFloat(EUR_K_COLUMN);
                float eur_s = cursor.getFloat(EUR_S_COLUMN);
                float usd_k = cursor.getFloat(USD_K_COLUMN);
                float usd_s = cursor.getFloat(USD_S_COLUMN);
                float gbp_k = cursor.getFloat(GBP_K_COLUMN);
                float gbp_s = cursor.getFloat(GBP_S_COLUMN);
                float chf_k = cursor.getFloat(CHF_K_COLUMN);
                float chf_s = cursor.getFloat(CHF_S_COLUMN);
                int grafika = cursor.getInt(GRAFIKA_COLUMN);

                waluta = new KursyRow(grafika, nazwa, eur_k, eur_s, usd_k, usd_s, gbp_k, gbp_s, chf_k, chf_s, null);
            }
        return waluta;
    }
}