package com.figielski.grzegorz.porownywarkakantorow.bazadanych;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.figielski.grzegorz.porownywarkakantorow.R;
import com.figielski.grzegorz.porownywarkakantorow.model.KantoryItem;

/**
 * Created by Grzesiek on 28.03.2017.
 */
public class KantoryDbAdapter {

    private SQLiteDatabase db;
    private Context context;
    private DatabaseHelper dbHelper;

    public KantoryDbAdapter(Context context) {                                    //konstruktor
        this.context = context;
    }

    /**
     *
     * ponizej stale uzywane do konstrukcji i poruszania sie po bazie danych
     *
     */

    private static final String DEBUG_TAG = "SqLiteTodoManager";
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "kantory.db";
    private static final String DB_KANTORY_TABLE = "kantory";

    private static final String KEY_ID = "id";                                       //kolumna ID kantoru
    private static final String ID_OPTIONS = "INTEGER PRIMARY KEY AUTOINCREMENT";
    private static final int ID_COLUMN = 0;
    private static final String KEY_NAZWA = "nazwa";                                 //kolumna nazwy
    private static final String NAZWA_OPTIONS = "TEXT NOT NULL UNIQUE";
    private static final int NAZWA_COLUMN = 1;
    private static final String KEY_URL = "url";                                     //adres www kantoru
    private static final String URL_OPTIONS = "TEXT NOT NULL";
    private static final int URL_COLUMN = 2;
    private static final String KEY_GRAFIKA = "grafika";                             //adres do grafiki kantoru
    private static final String GRAFIKA_OPTIONS = "INTEGER";
    private static final int GRAFIKA_COLUMN = 3;


    private static final String DB_CREATE_KANTORY_TABLE =                             //stworzenie tabeli
            "CREATE TABLE " + DB_KANTORY_TABLE + "( " +
                    KEY_ID + " " + ID_OPTIONS + ", " +
                    KEY_NAZWA + " " + NAZWA_OPTIONS + ", " +
                    KEY_URL + " " + URL_OPTIONS + ", " +
                    KEY_GRAFIKA + " " + GRAFIKA_OPTIONS +
                    ");";
    private static final String DROP_KANTORY_TABLE =                                   //usuniecie tabeli
            "DROP TABLE IF EXISTS " + DB_KANTORY_TABLE;

    /**
     *
     * klasa uzywana do tworzenia i aktualizowania bazy danych
     *
     */

    private static class DatabaseHelper extends SQLiteOpenHelper {
        private DatabaseHelper(Context context, String name,
                              SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        /**
         *
         * metoda uruchamiana przy tworzeniu bazy danych
         *
         */

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE_KANTORY_TABLE);

            Log.d(DEBUG_TAG, "Database creating...");                                           //logi widoczne w logcat'cie
            Log.d(DEBUG_TAG, "Table " + DB_KANTORY_TABLE + " ver." + DB_VERSION + " created");  //uzywane do debugowania

            //ponizej nastepuje wypelnienie bazy obslugiwanymi przez aplikacje kantorami

            ContentValues newValues = new ContentValues();
            newValues.put(KEY_ID, "1");
            newValues.put(KEY_NAZWA, "NBP");
            newValues.put(KEY_URL, "http://rss.nbp.pl/kursy/TabelaA.xml");
            newValues.put(KEY_GRAFIKA, R.raw.kantor_logo_nbp);
            db.insert(getDbKantoryTable(), null, newValues);

            ContentValues newValues2 = new ContentValues();
            newValues2.put(KEY_ID, "2");
            newValues2.put(KEY_NAZWA, "Walutomat");
            newValues2.put(KEY_URL, "https://stary.walutomat.pl/wymiana-walut");
            newValues2.put(KEY_GRAFIKA, R.raw.kantor_logo_walutomat);
            db.insert(getDbKantoryTable(), null, newValues2);

            ContentValues newValues3 = new ContentValues();
            newValues3.put(KEY_ID, "3");
            newValues3.put(KEY_NAZWA, "Cinkciarz");
            newValues3.put(KEY_URL, "http://www.cinkciarz.pl/");
            newValues3.put(KEY_GRAFIKA, R.raw.kantor_logo_cinkciarz);
            db.insert(getDbKantoryTable(), null, newValues3);

            ContentValues newValues4 = new ContentValues();
            newValues4.put(KEY_ID, "4");
            newValues4.put(KEY_NAZWA, "Kantor Alior");
            newValues4.put(KEY_URL, "https://m.kantor.aliorbank.pl/");
            newValues4.put(KEY_GRAFIKA, R.raw.kantor_logo_alior);
            db.insert(getDbKantoryTable(), null, newValues4);

            ContentValues newValues5 = new ContentValues();
            newValues5.put(KEY_ID, "5");
            newValues5.put(KEY_NAZWA, "Internetowy Kantor");
            newValues5.put(KEY_URL, "https://www.internetowykantor.pl/");
            newValues5.put(KEY_GRAFIKA, R.raw.kantor_logo_internetowy);
            db.insert(getDbKantoryTable(), null, newValues5);

            ContentValues newValues6 = new ContentValues();
            newValues6.put(KEY_ID, "6");
            newValues6.put(KEY_NAZWA, "Kantor Ekspert");
            newValues6.put(KEY_URL, "https://kantorekspert.pl/");
            newValues6.put(KEY_GRAFIKA, R.raw.kantor_logo_ekspert);
            db.insert(getDbKantoryTable(), null, newValues6);

        }

        /**
         *
         * metoda uruchamiana, gdy po aktualizacji aplikacji
         * baza danych ma wyzsza wersje niz przed aktualizacja
         *
         */

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(DROP_KANTORY_TABLE);

            Log.d(DEBUG_TAG, "Database updating...");
            Log.d(DEBUG_TAG, "Table " + DB_KANTORY_TABLE + " updated from ver." + oldVersion + " to ver." + newVersion);
            Log.d(DEBUG_TAG, "All data is lost.");

            onCreate(db);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * metoda uzywana do otwarcia dostepu do bazy danych
     *
     */

    public KantoryDbAdapter open(){
        dbHelper = new DatabaseHelper(context, DB_NAME, null, DB_VERSION);
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLException e) {
            db = dbHelper.getReadableDatabase();
        }
        return this;
    }

    /**
     *
     * metoda uzywana do zamkniecia dostepu do bazy danych
     *
     */

    public void close() {                                                       //zamkniecie bazy
        dbHelper.close();
    }

    /**
     *
     * zwraca cursor z wszystkimi kantorami z bazy
     *
     */

    public Cursor getAllKantory() {
        String[] columns = {KEY_ID, KEY_NAZWA, KEY_URL, KEY_GRAFIKA};
        return db.query(getDbKantoryTable(), columns, null, null, null, null, null);
    }

    /**
     *
     * zwraca kantor w postaci KantoryItem o danym id z bazy
     *
     */

    public KantoryItem getKantory(long id) {
        String[] columns = {KEY_ID, KEY_NAZWA, KEY_URL, KEY_GRAFIKA};
        String where = KEY_ID + "=" + id;
        Cursor cursor = db.query(getDbKantoryTable(), columns, where, null, null, null, null);
        KantoryItem kantory = null;
        if(cursor != null && cursor.moveToFirst()) {
            String nazwa = cursor.getString(NAZWA_COLUMN);
            String url = cursor.getString(URL_COLUMN);
            int grafika = cursor.getInt(GRAFIKA_COLUMN);
            kantory = new KantoryItem(id, nazwa, url, grafika);
        }
        return kantory;
    }

    public static String getDbKantoryTable() {
        return DB_KANTORY_TABLE;
    }
}
