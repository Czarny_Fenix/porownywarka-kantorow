package com.figielski.grzegorz.porownywarkakantorow.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.figielski.grzegorz.porownywarkakantorow.R;
import com.figielski.grzegorz.porownywarkakantorow.model.DialogListRow;

/**
 * Created by Grzesiek on 23.05.2017.
 */

public class DialogListAdapter extends ArrayAdapter<DialogListRow> {
    private Context context;
    private int layoutResourceId;
    private DialogListRow data[] = null;

    public DialogListAdapter(Context context, int layoutResourceId, DialogListRow[] data){
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        DialogListHolder holder;
        if(row==null){
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new DialogListAdapter.DialogListHolder();
            holder.nazwa = (TextView) row.findViewById(R.id.dialog_list_textview);
            row.setTag(holder);
        } else{
            holder = (DialogListHolder) row.getTag();
        }
        DialogListRow object = data[position];
        holder.nazwa.setText(object.nazwa);
        return row;
    }

    public class DialogListHolder{
        TextView nazwa;
    }
}
