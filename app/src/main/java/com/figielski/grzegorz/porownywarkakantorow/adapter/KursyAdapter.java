package com.figielski.grzegorz.porownywarkakantorow.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.figielski.grzegorz.porownywarkakantorow.R;
import com.figielski.grzegorz.porownywarkakantorow.model.KursyRow;

/**
 * Created by Grzesiek on 11.04.2017.
 */

public class KursyAdapter extends ArrayAdapter<KursyRow> {
    private Context context;
    private int layoutResourceId;
    private KursyRow data[] = null;


    public KursyAdapter(Context context, int layoutResourceId, KursyRow[] data){
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        KursyRowHolder holder;
        if(row==null){
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new KursyRowHolder();
            holder.logoKantoru = (ImageView) row.findViewById(R.id.logo_kantoru);
            holder.nazwaKantoru = (TextView)row.findViewById(R.id.nazwa_kantoru);
            holder.kurs_k_eur = (TextView)row.findViewById(R.id.kurs_k_eur);
            holder.kurs_s_eur = (TextView)row.findViewById(R.id.kurs_s_eur);
            holder.kurs_k_usd = (TextView)row.findViewById(R.id.kurs_k_usd);
            holder.kurs_s_usd = (TextView)row.findViewById(R.id.kurs_s_usd);
            holder.kurs_k_gbp = (TextView)row.findViewById(R.id.kurs_k_gbp);
            holder.kurs_s_gbp = (TextView)row.findViewById(R.id.kurs_s_gbp);
            holder.kurs_k_chf = (TextView)row.findViewById(R.id.kurs_k_chf);
            holder.kurs_s_chf = (TextView)row.findViewById(R.id.kurs_s_chf);
            row.setTag(holder);
        } else {
            holder = (KursyRowHolder)row.getTag();
        }
        KursyRow object = data[position];
        holder.nazwaKantoru.setText(object.nazwa);
        holder.logoKantoru.setImageResource(object.logo);

        holder.kurs_k_eur.setText(wypelniacz(object.eur_k));
        holder.kurs_s_eur.setText(wypelniacz(object.eur_s));
        holder.kurs_k_usd.setText(wypelniacz(object.usd_k));
        holder.kurs_s_usd.setText(wypelniacz(object.usd_s));
        holder.kurs_k_gbp.setText(wypelniacz(object.gbp_k));
        holder.kurs_s_gbp.setText(wypelniacz(object.gbp_s));
        holder.kurs_k_chf.setText(wypelniacz(object.chf_k));
        holder.kurs_s_chf.setText(wypelniacz(object.chf_s));

        return row;
    }

    static class KursyRowHolder{
        ImageView logoKantoru;
        TextView nazwaKantoru;
        TextView kurs_k_eur;
        TextView kurs_s_eur;
        TextView kurs_k_usd;
        TextView kurs_s_usd;
        TextView kurs_k_gbp;
        TextView kurs_s_gbp;
        TextView kurs_k_chf;
        TextView kurs_s_chf;
    }

    private String wypelniacz(float arg){
        String kurs=String.valueOf(arg);
        int index = kurs.indexOf(".");
        while(kurs.substring(index).length()<5){
            kurs = kurs + "0";
        }
        return kurs;
    }
}
