package com.figielski.grzegorz.porownywarkakantorow;

import android.content.Context;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.URL;

import static java.lang.Float.parseFloat;

/**
 * Created by Grzesiek on 05.04.2017.
 */

/** parser na podstawie https://www.tutorialspoint.com/android/android_rss_reader.htm **/

public class ParseXML {

    private static ParseXML Instance = null;
    private static Context context;

    static ParseXML getInstance(Context context) {
        if(Instance == null){
            Instance = new ParseXML(context);
        }
        return Instance;
    }

    ParseXML(Context context){
        this.context = context;
        eurNBP = context.getResources().getString(R.string.nbpeur);
        usdNBP = context.getResources().getString(R.string.nbpusd);
        chfNBP = context.getResources().getString(R.string.nbpchf);
        gbpNBP = context.getResources().getString(R.string.nbpgbp);
    }

    private String eurNBP;
    private String usdNBP;
    private String chfNBP;
    private String gbpNBP;
    private int eurpos;
    private int usdpos;
    private int chfpos;
    private int gbppos;
    private float kursEur;
    private float kursUsd;
    private float kursChf;
    private float kursGbp;


    public float getKursEur() {
        return kursEur;
    }

    public float getKursUsd() {
        return kursUsd;
    }

    public float getKursChf() {
        return kursChf;
    }

    public float getKursGbp() {
        return kursGbp;
    }

    /**
     *
     * @param url           - adres zrodla
     *
     * metoda majaca za zadanie pobrac zrodlo url dla NBP, a nastepnie wyszukac pozycje najswiezszych kursow
     * w pobranym materiale i uruchomic metode kursyWalutNBP, ktora te kursy wydobedzie z materialu zrodlowego
     * i wpisze jako element klasy
     *
     */

    public void walutyNBP(URL url){
        cleaner();
        String text = "";
        try{
            InputStream in = url.openStream();
            XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
            XmlPullParser myparser = xmlFactoryObject.newPullParser();
            myparser.setInput(in, null);
            int event = myparser.getEventType();

            while (event != XmlPullParser.END_DOCUMENT) {

                switch (event){

                    case XmlPullParser.TEXT:
                        text = myparser.getText();
                        break;
                }

                if(text.toLowerCase().contains(eurNBP.toLowerCase())){
                    this.eurpos = text.indexOf(eurNBP);
                    this.usdpos = text.indexOf(usdNBP);
                    this.chfpos = text.indexOf(chfNBP);
                    this.gbppos = text.indexOf(gbpNBP);
                    kursyWalutNBP(text);
                    break;
                }
                event = myparser.next();
            }
        } catch(Exception e){
            Log.e(context.getResources().getString(R.string.zlapano_exception), context.getResources().getString(R.string.parsexml));
            e.printStackTrace();
        }
    }

    /**
     *
     * metoda wycinajaca interesujace kursy z parametru description i wpisujaca ich wartosci do zmiennych klasy
     *
     */

    private void kursyWalutNBP(String description){
        int index;
        int index2;
        char przecinek = ',';
        char kropka = '.';
        index = description.indexOf(przecinek, eurpos);
        index2 = index;
        while (description.substring(index-1, index).matches(context.getResources().getString(R.string.pattern_cyfra))){
            index--;
        }
        this.kursEur=parseFloat(description.substring(index,index2+4).replace(przecinek,kropka));

        index = description.indexOf(przecinek, usdpos);
        index2 = index;
        while (description.substring(index-1, index).matches(context.getResources().getString(R.string.pattern_cyfra))){
            index--;
        }
        this.kursUsd=parseFloat(description.substring(index,index2+4).replace(przecinek,kropka));

        index = description.indexOf(przecinek, gbppos);
        index2 = index;
        while (description.substring(index-1, index).matches(context.getResources().getString(R.string.pattern_cyfra))){
            index--;
        }
        this.kursGbp=parseFloat(description.substring(index,index2+4).replace(przecinek,kropka));

        index = description.indexOf(przecinek, chfpos);
        index2 = index;
        while (description.substring(index-1, index).matches(context.getResources().getString(R.string.pattern_cyfra))){
            index--;
        }
        this.kursChf=parseFloat(description.substring(index,index2+4).replace(przecinek,kropka));
    }

    /**
     *
     * metoda uzywana do wyczyszczenia wartosci kursow w klasie, oraz indeksow znalezionych znacznikow
     *
     */

    private void cleaner(){
        this.kursEur  = -1;
        this.eurpos   = -1;
        this.kursUsd  = -1;
        this.usdpos   = -1;
        this.kursChf  = -1;
        this.chfpos   = -1;
        this.kursGbp  = -1;
        this.gbppos   = -1;
    }
}
