package com.figielski.grzegorz.porownywarkakantorow.model;


/**
 * Created by Grzesiek on 23.05.2017.
 */

public class DialogListRow {
    public String nazwa;
    private int wartosc;

    public DialogListRow(String nazwa, int wartosc){
        this.nazwa = nazwa;
        this.wartosc = wartosc;
    }

    public int getWartosc(){
        return wartosc;
    }

    public String getNazwa() {
        return nazwa;
    }
}
