package com.figielski.grzegorz.porownywarkakantorow;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.figielski.grzegorz.porownywarkakantorow.bazadanych.KantoryDbAdapter;
import com.figielski.grzegorz.porownywarkakantorow.bazadanych.KursyDbAdapter;
import com.figielski.grzegorz.porownywarkakantorow.model.KantoryItem;
import com.figielski.grzegorz.porownywarkakantorow.model.KursyItem;

import java.sql.Timestamp;

/**
 * Created by Grzesiek on 20.05.2017.
 */

public class DatabaseInserter {

    private KursyDbAdapter kursydb;
    private KantoryDbAdapter kantorydb;
    private ConnectivityManager conMgr;

    private static DatabaseInserter Instance = null;
    private static Context context;

    static DatabaseInserter getInstance(Context context) {
        if(Instance == null){
            Instance = new DatabaseInserter(context);
        }
        return Instance;
    }

    private DatabaseInserter(Context context){
        this.context=context;
        kursydb=new KursyDbAdapter(context);
        kantorydb=new KantoryDbAdapter(context);
        conMgr = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    /**
     *
     * metoda dodajaca nowe kursy do lokalnej bazy danych
     *
     */

    public void aktualizacjaBazy(){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);    //dostep do ustawien
        String tmp;                                                                         //zmienna uzywana do sprawdzenia czy uzytkownik wylaczyl odswiezanie danego kantoru
        Boolean czy_odswiezac_kantor;                                                       //wartosc pola SharedPreferences czy uzytkowniok chce odswiezenia danych dla danego kantoru
        KantoryItem kantor;                                                                 //biezacy kantor, ktory jest sprawdzany i odswiezany
        KursyItem kursy;                                                                    //zmienna przechowujaca rekord do wpisania do bazy lokalnej
        Cursor kantory;                                                                     //Cursor przechowujacy wszystkie kantory z bazy lokalnej
        SharedPreferences.Editor prefEdytor = pref.edit();                                  //dostep do edycji ustawien (na potrzeby wyswietlania czasu reakcji zrodla, oraz ilosci rekordow w bazie lokalnej)
        Long czasA;                                                                         //czas rozpoczecia aktualizacji biezacego zrodla
        Long czasB;                                                                         //czas zakonczenia aktualizacji biezacego zrodla
        Timestamp czasAkcji;                                                                //czas aktualizacji biezacego zrodla
        int ilosc;                                                                          //ilosc kantorow do obsluzenina
        int i;                                                                              //iteracja petli
        int blad = 0;                                                                       //ilosc blednie obsluzonych zrodel
        int ilosc_aktualizowanych;                                                          //ilosc zrodel, ktore powinny zostac zaktualizowane

        if(isInternetAvailable()){
            try{
                Long time = System.currentTimeMillis();
                kantorydb.open();
                kursydb.open();
                kantory = kantorydb.getAllKantory();
                ilosc = kantory.getCount();
                ilosc_aktualizowanych = ilosc;
                for(i=1; i<=ilosc; i++){
                    kantor = kantorydb.getKantory(i);
                    tmp = kantor.getNazwa();
                    tmp = tmp.toLowerCase();
                    tmp = tmp.replace(context.getResources().getString(R.string.spacja), context.getResources().getString(R.string.podloga));
                    czy_odswiezac_kantor = pref.getBoolean(tmp + context.getResources().getString(R.string.switch_preference), true);
                    if(!czy_odswiezac_kantor){
                        ilosc_aktualizowanych--;
                        continue;
                    }
                    czasA = System.currentTimeMillis();
                    ParseHTML.getInstance(context).waluty(kantor.getUrl());
                    if(ParseHTML.getInstance(context).isBlad()){
                        blad++;
                        continue;
                    }
                    kursy = new KursyItem(i, context.getResources().getString(R.string.eur), ParseHTML.getInstance(context).getEur(), time);
                    kursydb.insertKursy(kursy);
                    kursy = new KursyItem(i, context.getResources().getString(R.string.usd), ParseHTML.getInstance(context).getUsd(), time);
                    kursydb.insertKursy(kursy);
                    kursy = new KursyItem(i, context.getResources().getString(R.string.chf), ParseHTML.getInstance(context).getChf(), time);
                    kursydb.insertKursy(kursy);
                    kursy = new KursyItem(i, context.getResources().getString(R.string.gbp), ParseHTML.getInstance(context).getGbp(), time);
                    kursydb.insertKursy(kursy);
                    czasB = System.currentTimeMillis();
                    czasAkcji = new Timestamp(czasB-czasA);
                    prefEdytor.putString(tmp + context.getResources().getString(R.string.text_preference),czasAkcji.toString().substring(czasAkcji.toString().indexOf(":")+1));
                    prefEdytor.commit();
                }
                kantorydb.close();
                prefEdytor.putString( context.getResources().getString(R.string.ilosc_rekordow_text), Long.toString(kursydb.getIloscRekordow()));
                prefEdytor.commit();
                kursydb.close();
                if(blad==ilosc_aktualizowanych){
                    CharSequence text = context.getResources().getString(R.string.wystapil_blad_calkowity);
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                } else if(blad!=0){
                    CharSequence text = context.getResources().getString(R.string.wystapil_blad_czesciowy);
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
            } catch (Exception e){
                Log.e(context.getResources().getString(R.string.zlapano_exception), context.getResources().getString(R.string.databaseinserter));
            }
        }

    }

    public boolean isInternetAvailable()
    {
        NetworkInfo i = conMgr.getActiveNetworkInfo();
        if (i == null) return false;
        if (!i.isConnected()) return false;
        if (!i.isAvailable()) return false;
        return true;
    }
}
