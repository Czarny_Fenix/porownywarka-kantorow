package com.figielski.grzegorz.porownywarkakantorow.fragmenty;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.figielski.grzegorz.porownywarkakantorow.R;

import java.sql.Timestamp;

/**
 * Created by Grzesiek on 22.05.2017.
 */

public class GraphFragment extends Fragment {

    public Button button_graphu;
    private TextView interwal_czasowy;
    private WebView wykres_view;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_graph, container, false);
        context = rootView.getContext();

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(context.getResources().getString(R.string.wykres_kursow_tytul));

        button_graphu = (Button) rootView.findViewById(R.id.button_wykresu);
        button_graphu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pokazDialog();
            }
        });
        wykres_view = (WebView) rootView.findViewById(R.id.wykres);
        wykres_view.getSettings().setJavaScriptEnabled(true);
        interwal_czasowy = (TextView) rootView.findViewById(R.id.wykres_textview);
        wyswietlOstatniWykres(rootView.getContext());

        return rootView;
    }

    /**
     *
     * metoda wyswietlajaca Dialog z wyborem zakresu wykresow
     *
     */

    public void pokazDialog(){
        ListaDialogFragment dialog = new ListaDialogFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        dialog.setTargetFragment(this, 0);
        dialog.show(ft, context.getResources().getString(R.string.lista_dialog));
    }

    /**
     *
     * metoda wyswietlajaca wykresy dla ostatnio przegladanego zakresu czasu
     *
     */

    private void wyswietlOstatniWykres(Context context){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        String ostatnie_zapytanie = pref.getString(context.getResources().getString(R.string.ostatni_wykres), "3");
        String dni_slownie;
        int ilosc_dni = Integer.parseInt(ostatnie_zapytanie);

        switch (ilosc_dni){
            case 3:
                dni_slownie = context.getString(R.string.ostatnie_dni_trzy);
                break;
            case 7:
                dni_slownie = context.getString(R.string.ostatnie_dni_tydzien);
                break;
            case 30:
                dni_slownie = context.getString(R.string.ostatnie_dni_miesiac);
                break;
            case 90:
                dni_slownie = context.getString(R.string.ostatnie_dni_kwartal);
                break;
            default:
                dni_slownie = context.getString(R.string.ostatnie_dni_rok);
                break;
        }

        wyswietlWykres(ilosc_dni, dni_slownie);
    }

    /**
     *
     * metoda aktualizujaca ostatnio wyswietlany wykres
     *
     */

    public void pobierzWykres(int ilosc_dni, String ilosc_dni_slownie){
        String data_zero = context.getResources().getString(R.string.data_zero);
        boolean czy_poprawna_data=true;
        Long time = System.currentTimeMillis();
        Long a = new Long("86400000");
        Long b = new Long(ilosc_dni);
        Long interwal = a*b;
        Timestamp timestamp = new Timestamp(time-interwal);
        String czas = timestamp.toString().substring(0, timestamp.toString().indexOf(" "));

        if(Integer.parseInt(czas.substring(0,4))<Integer.parseInt(data_zero.substring(0,4))){
            czy_poprawna_data=false;
        } else if(Integer.parseInt(czas.substring(0,4))==Integer.parseInt(data_zero.substring(0,4)) &&
                Integer.parseInt(czas.substring(5,7))<Integer.parseInt(data_zero.substring(5,7))){
            czy_poprawna_data=false;
        } else if(Integer.parseInt(czas.substring(0,4))==Integer.parseInt(data_zero.substring(0,4)) &&
                Integer.parseInt(czas.substring(5,7))==Integer.parseInt(data_zero.substring(5,7)) &&
                Integer.parseInt(czas.substring(8,10))<Integer.parseInt(data_zero.substring(8,10))){
            czy_poprawna_data=false;
        }

        if(czy_poprawna_data){
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor prefEdytor = pref.edit();
            prefEdytor.putString(context.getResources().getString(R.string.ostatni_wykres), Integer.toString(ilosc_dni));
            prefEdytor.commit();

            wyswietlWykres(ilosc_dni, ilosc_dni_slownie);
        } else {
            CharSequence text = context.getResources().getString(R.string.brak_historii)+ data_zero;
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(getActivity(), text, duration);
            toast.show();
        }
    }

    /**
     *
     * metoda wyswietlajaca wykres dla podanych parametrow
     *
     */

    public void wyswietlWykres(int ilosc_dni, String ilosc_dni_slownie){
        interwal_czasowy.setText(context.getResources().getString(R.string.wykres_dla) + ilosc_dni_slownie);
        wykres_view.loadUrl(context.getResources().getString(R.string.link_do_wykresow) + Integer.toString(ilosc_dni));
    }
}

