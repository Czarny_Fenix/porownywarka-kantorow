package com.figielski.grzegorz.porownywarkakantorow.fragmenty;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.figielski.grzegorz.porownywarkakantorow.R;
import com.figielski.grzegorz.porownywarkakantorow.RemoteDatabaseHandler;
import com.figielski.grzegorz.porownywarkakantorow.adapter.KursyAdapter;
import com.figielski.grzegorz.porownywarkakantorow.bazadanych.RemoteDataDbAdapter;
import com.figielski.grzegorz.porownywarkakantorow.model.KursyRow;

import java.sql.Timestamp;

/**
 * Created by Grzesiek on 22.05.2017.
 */

public class RemoteDataFragment extends Fragment{

    private Button kalendarz_button;
    private TextView data;
    private String data_zapytania;
    public ProgressDialog mProgressDialog;
    private ListView lista_kursow_zewnetrznych;
    private Context context;
    private ConnectivityManager conMgr;

    public void setData_zapytania(String data_zapytania) {
        this.data_zapytania = data_zapytania;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_remote_data, container, false);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(rootView.getContext().getResources().getString(R.string.remote_data_title));


        kalendarz_button = (Button) rootView.findViewById(R.id.date_button);
        data = (TextView) rootView.findViewById(R.id.kursy_dla_daty_textview);
        context = rootView.getContext();
        conMgr = (ConnectivityManager)rootView.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        lista_kursow_zewnetrznych = (ListView)rootView.findViewById(R.id.ListaKursowZewnetrznych);
        kalendarz_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pokazDialog();
            }
        });
        wyswietlListe();
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this.getActivity());

        return rootView;
    }

    /**
     *
     * @param rok           - rok dla zapytania
     * @param miesiac       - miesiac dla zapytania
     * @param dzien         - dzien dla zapytania
     *
     * metoda nadpisująca date dla ostatniego zapytania do zewnetrznej bazy danych w SharedPreferences
     * oraz uruchamiajaca AsyncTask'a, ktory takie zapytanie zrealizuje
     *
     */

    public void ustawDate(int rok, int miesiac, int dzien){
        short czy_poprawna_data=1;
        Long time = System.currentTimeMillis();
        Timestamp timestamp = new Timestamp(time);
        String data_zero = context.getResources().getString(R.string.data_zero);
        String czas = timestamp.toString().substring(0, timestamp.toString().indexOf(" "));

        if(rok>Integer.parseInt(czas.substring(0,4))){
            czy_poprawna_data=2;
        } else if(rok==Integer.parseInt(czas.substring(0,4)) && miesiac>Integer.parseInt(czas.substring(5,7))){
            czy_poprawna_data=2;
        } else if(rok==Integer.parseInt(czas.substring(0,4)) && miesiac==Integer.parseInt(czas.substring(5,7)) && dzien>Integer.parseInt(czas.substring(8,10))){
            czy_poprawna_data=2;
        } else if(rok<Integer.parseInt(data_zero.substring(0,4))){
            czy_poprawna_data=0;
        } else if(rok==Integer.parseInt(data_zero.substring(0,4)) && miesiac<Integer.parseInt(data_zero.substring(5,7))){
            czy_poprawna_data=0;
        } else if(rok==Integer.parseInt(data_zero.substring(0,4)) && miesiac==Integer.parseInt(data_zero.substring(5,7)) && dzien<Integer.parseInt(data_zero.substring(8,10))){
            czy_poprawna_data=0;
        }

        if(czy_poprawna_data==0){
            CharSequence text = context.getResources().getString(R.string.brak_historii)+ data_zero;
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(getActivity(), text, duration);
            toast.show();
        } else if(czy_poprawna_data==2){
            CharSequence text = context.getString(R.string.bledna_data);
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(getActivity(), text, duration);
            toast.show();
        } else

        if(isInternetAvailable()){

            Context context = getActivity().getApplicationContext();
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor prefEdytor = pref.edit();
            prefEdytor.putString(context.getResources().getString(R.string.ostatnie_zapytanie), this.getResources().getString(R.string.sredni_kurs_dla_dnia) +
                    Integer.toString(dzien) + "." +
                    Integer.toString(miesiac) + "." +
                    Integer.toString(rok));
            prefEdytor.commit();


            String tmp;
            String miesiacString;
            String dzienString;
            miesiacString = Integer.toString(miesiac);
            dzienString = Integer.toString(dzien);
            if(miesiacString.length()==1){
                tmp = miesiacString;
                miesiacString = "0" + tmp;
            }
            if(dzienString.length()==1){
                tmp = dzienString;
                dzienString = "0" + tmp;
            }
            setData_zapytania(Integer.toString(rok) + "-" + miesiacString + "-" + dzienString);
            new WalutyZdalne().execute();
        } else {
            CharSequence text = context.getResources().getString(R.string.blad_polaczenia);
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(getActivity(), text, duration);
            toast.show();
        }
    }

    /**
     *
     * metoda wyswietlajaca ListView z kursami
     *
     */

    public void wyswietlListe(){
        RemoteDataDbAdapter bazaremote = new RemoteDataDbAdapter(getActivity().getApplicationContext());

        bazaremote.open();
        int iloscRekordow = bazaremote.getIloscRekordow();
        if(iloscRekordow!=0){
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this.getActivity());
            final KursyRow kursy_data[] = new KursyRow[iloscRekordow];
            for(int i=0; i<iloscRekordow; i++){
                kursy_data[i] = bazaremote.getRowAtPosiotion(i+1);
            }
            final KursyAdapter adapter = new KursyAdapter(context, R.layout.main_list_row, kursy_data);
            lista_kursow_zewnetrznych.setAdapter(adapter);
            data.setText(pref.getString(context.getResources().getString(R.string.ostatnie_zapytanie), this.getResources().getString(R.string.spacja)));
        }
        bazaremote.close();
    }

    /**
     *
     * metoda uruchamiajaca Dialog z wyborem daty
     *
     */

    public void pokazDialog(){
        KalendarzDialogFragment dialog = new KalendarzDialogFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        dialog.setTargetFragment(this, 0);
        dialog.show(ft, context.getResources().getString(R.string.kalendarz_dialog));
    }

    /**
     *
     * zadanie asynchroniczne majace na celu pobranie danych z zewnetrznej bazy,
     * zeby nastepnie wpisac/nadpisac je w lokalnym odzwierciedleniu fragmentu tej bazy
     *
     */

    private class WalutyZdalne extends AsyncTask<Void, Void, Void>{

        /**
         *
         * metoda uruchamiana przed wykonaniem AsyncTaska
         *
         */

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setTitle(context.getResources().getString(R.string.waluty));
            mProgressDialog.setMessage(context.getResources().getString(R.string.ladowanie));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            int orientacja = getResources().getConfiguration().orientation;
            if(orientacja== Configuration.ORIENTATION_PORTRAIT){
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            } else{
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        }

        /**
         *
         * metoda uruchamiana po wykonaniu AsyncTaska
         *
         */

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
            wyswietlListe();
            mProgressDialog.dismiss();
        }

        /**
         *
         * metoda zajmujaca sie polaczeniami / liczeniem w tle
         *
         */

        @Override
        protected Void doInBackground(Void... params) {
            if(isInternetAvailable()){
                RemoteDatabaseHandler.getInstance(getActivity()).kursyRemote(data_zapytania);
            } else {
                Context context = getActivity().getApplicationContext();
                CharSequence text = context.getResources().getString(R.string.blad_polaczenia);
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
            return null;
        }
    }

    /**
     *
     * metoda sprawdzajaca czy uzytkownik ma w pelni funkcjonalne polaczenie
     * z internetem
     *
     */

    public boolean isInternetAvailable()
    {
        NetworkInfo i = conMgr.getActiveNetworkInfo();
        if (i == null) return false;
        if (!i.isConnected()) return false;
        if (!i.isAvailable()) return false;
        return true;
    }

}
