package com.figielski.grzegorz.porownywarkakantorow.model;

/**
 * Created by Grzesiek on 11.04.2017.
 */

public class KursyRow {

    public int logo;
    public String nazwa;
    public float eur_k;
    public float eur_s;
    public float usd_k;
    public float usd_s;
    public float gbp_k;
    public float gbp_s;
    public float chf_k;
    public float chf_s;
    public String url;
    private static final String SUFIX = ".pl";

    public KursyRow(int logo, String nazwa, float eur_k, float eur_s,
                    float usd_k, float usd_s, float gbp_k, float gbp_s, float chf_k, float chf_s, String url) {
        this.logo = logo;
        this.nazwa = nazwa;
        this.eur_k = eur_k;
        this.eur_s = eur_s;
        this.usd_k = usd_k;
        this.usd_s = usd_s;
        this.gbp_k = gbp_k;
        this.gbp_s = gbp_s;
        this.chf_k = chf_k;
        this.chf_s = chf_s;
        this.url = url;
    }

    /**
     *
     * metoda zwracajaca skrocony url
     *
     */

    public String getFixedUrl(){
        String url = this.url;
        int index = url.indexOf(SUFIX);
        url = url.substring(0,index+4);
        return url;
    }
}
