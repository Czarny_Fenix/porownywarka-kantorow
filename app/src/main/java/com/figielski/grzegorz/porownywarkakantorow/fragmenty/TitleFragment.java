package com.figielski.grzegorz.porownywarkakantorow.fragmenty;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.figielski.grzegorz.porownywarkakantorow.R;

/**
 * Created by Grzesiek on 14.05.2017.
 */

public class TitleFragment extends Fragment{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_title, container, false);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(rootView.getContext().getResources().getString(R.string.strona_tytulowa_title));

        return rootView;
    }

}
