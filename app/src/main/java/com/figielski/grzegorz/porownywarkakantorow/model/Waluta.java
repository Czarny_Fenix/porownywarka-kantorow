package com.figielski.grzegorz.porownywarkakantorow.model;

/**
 * Created by Grzesiek on 10.05.2017.
 */

public class Waluta {
    private float k;
    private float s;

    public Waluta(float k, float s) {
        this.k = k;
        this.s = s;
    }

    public float getK() {
        return k;
    }

    public float getS() {
        return s;
    }
}
