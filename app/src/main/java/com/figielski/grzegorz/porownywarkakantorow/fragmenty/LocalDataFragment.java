package com.figielski.grzegorz.porownywarkakantorow.fragmenty;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.figielski.grzegorz.porownywarkakantorow.R;
import com.figielski.grzegorz.porownywarkakantorow.adapter.KursyAdapter;
import com.figielski.grzegorz.porownywarkakantorow.bazadanych.KantoryDbAdapter;
import com.figielski.grzegorz.porownywarkakantorow.bazadanych.KursyDbAdapter;
import com.figielski.grzegorz.porownywarkakantorow.model.KantoryItem;
import com.figielski.grzegorz.porownywarkakantorow.model.KursyRow;

import java.sql.Timestamp;

/**
 * Created by Grzesiek on 14.05.2017.
 */

public class LocalDataFragment extends Fragment{
    private ListView kursyView;
    private long timestamp;
    private TextView timestampView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(rootView.getContext().getResources().getString(R.string.kursy_lokalne_title));

        KursyDbAdapter kursydb=new KursyDbAdapter(getActivity().getApplicationContext());
        KantoryDbAdapter kantorydb=new KantoryDbAdapter(getActivity().getApplicationContext());

        kursydb.open();
        kantorydb.open();
        Cursor kolumna_kantorow = kursydb.getKID();
        kolumna_kantorow.moveToFirst();
        Cursor[] zapytanie = new Cursor[4];
        KantoryItem kantor;
        Boolean czy_odswiezac_kantor;
        int ilosc_kantorow_do_wyswietlenia=0;
        int ilosc = kolumna_kantorow.getCount();
        for(int i=1; i<=ilosc; i++){
            czy_odswiezac_kantor = czy_odswiezyc_kantor(i);
            if(czy_odswiezac_kantor){
                ilosc_kantorow_do_wyswietlenia++;
            }
        }
        int[] idkantorow = new int[ilosc_kantorow_do_wyswietlenia];
        for(int i=1, j=0; i<=ilosc; i++){
            czy_odswiezac_kantor = czy_odswiezyc_kantor(i);
            if(czy_odswiezac_kantor){
                idkantorow[j] = kolumna_kantorow.getInt(0);
                j++;
            }
            kolumna_kantorow.moveToNext();
        }
        final KursyRow kursy_data[]= new KursyRow[ilosc_kantorow_do_wyswietlenia];

        for(int i=0; i<ilosc_kantorow_do_wyswietlenia; i++){
            kantor = kantorydb.getKantory(idkantorow[i]);
            zapytanie[0] = kursydb.getNajnowszyKurs(idkantorow[i], this.getResources().getString(R.string.eur));
            zapytanie[1] = kursydb.getNajnowszyKurs(idkantorow[i], this.getResources().getString(R.string.usd));
            zapytanie[2] = kursydb.getNajnowszyKurs(idkantorow[i], this.getResources().getString(R.string.gbp));
            zapytanie[3] = kursydb.getNajnowszyKurs(idkantorow[i], this.getResources().getString(R.string.chf));
            zapytanie[0].moveToFirst();
            zapytanie[1].moveToFirst();
            zapytanie[2].moveToFirst();
            zapytanie[3].moveToFirst();
            kursy_data[i] = new KursyRow(kantor.getGrafika(), kantor.getNazwa(),
                     zapytanie[0].getFloat(2), zapytanie[0].getFloat(3),
                     zapytanie[1].getFloat(2), zapytanie[1].getFloat(3),
                     zapytanie[2].getFloat(2), zapytanie[2].getFloat(3),
                     zapytanie[3].getFloat(2), zapytanie[3].getFloat(3), kantor.getUrl());
        }

        timestamp=kursydb.getNajnowszyTimestamp();
        kursydb.close();
        kantorydb.close();

        final KursyAdapter adapter = new KursyAdapter(rootView.getContext(), R.layout.main_list_row, kursy_data);
        kursyView = (ListView)rootView.findViewById(R.id.ListaKursow);
        timestampView= (TextView)rootView.findViewById(R.id.timestampBazaLokalna);
        kursyView.setAdapter(adapter);

        kursyView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String url = kursy_data[position].getFixedUrl();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        if (timestamp==0){} else{
            Timestamp czas = new Timestamp(timestamp);
            timestampView.setText(this.getResources().getString(R.string.aktualizacja) + czas.toString());
        }
        return rootView;
    }

    /**
     *
     * @param id    - identyfikator kantoru w bazie danych
     *
     * metoda zwracajaca wartosc z sharedpreferences przechowujaca
     * boole'a czy odwiedzic zrodlo o identyfikatorze w bazie id
     *
     */

    private boolean czy_odswiezyc_kantor(int id){
        String nazwa_preferencji;
        KantoryItem kantor;
        Boolean czy_odswiezac_kantor;
        KantoryDbAdapter kantorydb=new KantoryDbAdapter(getActivity().getApplicationContext());
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this.getActivity());

        kantorydb.open();
        kantor = kantorydb.getKantory(id);
        kantorydb.close();
        nazwa_preferencji = kantor.getNazwa();
        nazwa_preferencji = nazwa_preferencji.toLowerCase();
        nazwa_preferencji = nazwa_preferencji.replace(this.getResources().getString(R.string.spacja), this.getResources().getString(R.string.podloga));
        nazwa_preferencji = nazwa_preferencji + this.getResources().getString(R.string.switch_preference);
        czy_odswiezac_kantor = pref.getBoolean(nazwa_preferencji, true);
        return czy_odswiezac_kantor;
    }
}
