package com.figielski.grzegorz.porownywarkakantorow.model;

/**
 * Created by Grzesiek on 26.04.2017.
 */

public class KantoryItem {

    private long id;
    private String nazwa;
    private String url;
    private int grafika;

    public KantoryItem(long id, String nazwa, String url, int grafika){
        this.id=id;
        this.nazwa=nazwa;
        this.url=url;
        this.grafika=grafika;
    }

    public long getId() {
        return id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public String getUrl() {
        return url;
    }

    public int getGrafika() {
        return grafika;
    }

}
