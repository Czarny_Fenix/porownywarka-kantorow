package com.figielski.grzegorz.porownywarkakantorow.bazadanych;

/**
 * Created by Grzesiek on 27.03.2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.util.Log;

import com.figielski.grzegorz.porownywarkakantorow.R;
import com.figielski.grzegorz.porownywarkakantorow.model.KursyItem;

/**
 SQLite na podstawie http://www.android4devs.pl/2011/07/sqlite-androidzie-kompletny-poradnik-dla-poczatkujacych/
 **/

public class KursyDbAdapter {

    private SQLiteDatabase db;
    private Context context;
    private DatabaseHelper dbHelper;

    public KursyDbAdapter(Context context) {                                    //konstruktor
        this.context = context;
    }

    /**
     *
     * ponizej stale uzywane do konstrukcji i poruszania sie po bazie danych
     *
     */

    private static final String DEBUG_TAG = "SqLiteTodoManager";
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "kursy.db";
    private static final String DB_KURSY_TABLE = "kursy";

    private static final String KEY_ID = "id";                                       //kolumna ID
    private static final String ID_OPTIONS = "INTEGER PRIMARY KEY AUTOINCREMENT";
    private static final int ID_COLUMN = 0;
    private static final String KEY_IDK = "idk";                                     //kolumna ID kantoru
    private static final String IDK_OPTIONS = "INTEGER";
    private static final int IDK_COLUMN = 1;
    private static final String KEY_WALUTA = "waluta";                               //kolumna waluty
    private static final String WALUTA_OPTIONS = "TEXT NOT NULL";
    private static final int WALUTA_COLUMN = 2;
    private static final String KEY_KURSK = "kursk";                                 //kurs kupna
    private static final String KURSK_OPTIONS = "FLOAT NOT NULL";
    private static final int KURSK_COLUMN = 3;
    private static final String KEY_KURSS = "kurss";                                 //kurs sprzedazy
    private static final String KURSS_OPTIONS = "FLOAT NOT NULL";
    private static final int KURSS_COLUMN = 4;
    private static final String KEY_TIME = "time";                                   //timestamp
    private static final String TIME_OPTIONS = "LONG";
    private static final int TIME_COLUMN = 5;


    private static final String DB_CREATE_KURSY_TABLE =                             //stworzenie tabeli
            "CREATE TABLE " + DB_KURSY_TABLE + "( " +
                    KEY_ID + " " + ID_OPTIONS + ", " +
                    KEY_IDK + " " + IDK_OPTIONS + ", " +
                    KEY_WALUTA + " " + WALUTA_OPTIONS + ", " +
                    KEY_KURSK + " " + KURSK_OPTIONS + ", " +
                    KEY_KURSS + " " + KURSS_OPTIONS + ", " +
                    KEY_TIME + " " + TIME_OPTIONS +
                    ");";
    private static final String DROP_KURSY_TABLE =                                  //usuniecie tabeli
            "DROP TABLE IF EXISTS " + DB_KURSY_TABLE;


    /**
     *
     * klasa uzywana do tworzenia i aktualizowania bazy danych
     *
     */


    private static class DatabaseHelper extends SQLiteOpenHelper {
        private DatabaseHelper(Context context, String name,
                              SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        /**
         *
         * metoda uruchamiana przy tworzeniu bazy danych
         *
         */

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE_KURSY_TABLE);

            Log.d(DEBUG_TAG, "Database creating...");
            Log.d(DEBUG_TAG, "Table " + DB_KURSY_TABLE + " ver." + DB_VERSION + " created");

        }

        /**
         *
         * metoda uruchamiana, gdy po aktualizacji aplikacji
         * baza danych ma wyzsza wersje niz przed aktualizacja
         *
         */


        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(DROP_KURSY_TABLE);

            Log.d(DEBUG_TAG, "Database updating...");
            Log.d(DEBUG_TAG, "Table " + DB_KURSY_TABLE + " updated from ver." + oldVersion + " to ver." + newVersion);
            Log.d(DEBUG_TAG, "All data is lost.");

            onCreate(db);
        }
    }
    //////////////////////////////////////// METODY ////////////////////////////////////////

    /**
     *
     * metoda uzywana do otwarcia dostepu do bazy danych
     *
     */

    public KursyDbAdapter open(){
        dbHelper = new DatabaseHelper(context, DB_NAME, null, DB_VERSION);
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLException e) {
            db = dbHelper.getReadableDatabase();
        }
        return this;
    }

    /**
     *
     * metoda uzywana do zamkniecia dostepu do bazy danych
     *
     */

    public void close() {                                                       //zamkniecie bazy
        dbHelper.close();
    }

    private static String getDbKursyTable() {
        return DB_KURSY_TABLE;
    }


    /**
     *
     * metoda posrednia wywolywana w celu dodania nowego kursy do bazy
     *
     */


    public long insertKursy(KursyItem kursy){
        long idk=kursy.getIdk();
        String waluta=kursy.getNazwa();
        float wartosck=kursy.getWartosck();
        float wartoscs=kursy.getWartoscs();
        long time=kursy.getTimestamp();
        return insertKursy(idk, waluta, wartosck, wartoscs, time);
    }

    /**
     *
     * metoda dodajaca nowe kursy do bazy danych
     *
     */

    private long insertKursy(long idk, String waluta, float kursk, float kurss, long time){
        ContentValues newValues = new ContentValues();
        newValues.put(KEY_IDK, idk);
        newValues.put(KEY_WALUTA, waluta);
        newValues.put(KEY_KURSK, kursk);
        newValues.put(KEY_KURSS, kurss);
        newValues.put(KEY_TIME, time);
        return db.insert(getDbKursyTable(), null, newValues);
    }

    /**
     *
     * metoda zwracajaca id kantorow z bazy kursow
     *
     */

    public Cursor getKID() {
        String[] columns = {KEY_IDK};
        return db.query(true, getDbKursyTable(), columns, null, null, null, null, null, null);
    }

    /**
     *
     * @param idk       - id kantoru
     * @param waluta    - nazwa waluty
     *
     * metoda zwracajaca najnowszy kurs dla danego kantoru i danej waluty
     *
     */

    public Cursor getNajnowszyKurs(int idk, String waluta){
        String walutaVar = "'" + waluta + "'";
        String x ="SELECT " + KEY_IDK + ", " + KEY_WALUTA + ", " + KEY_KURSK + ", " +
        KEY_KURSS + ", " + KEY_TIME + " " + "FROM " + DB_KURSY_TABLE + " " + "WHERE " + KEY_IDK + " " + "= " +
                Integer.toString(idk) + " AND " + KEY_WALUTA + " = " + walutaVar + " ORDER BY time DESC";
        return db.rawQuery(x, null);
    }

    /**
     *
     * metoda usuwajaca stare kursy z bazy danych
     *
     */

    public long deleteStareKursy(Context context){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        String tmp = pref.getString(context.getResources().getString(R.string.delete_frequency), "7");
        int ilosc_dni = Integer.parseInt(tmp);
        Long time = System.currentTimeMillis() - (1000 * 60 * 60 * 24 * ilosc_dni);
        String tydzien = time.toString();
        String[] arg = new String[] {tydzien};

        return db.delete(DB_KURSY_TABLE, "time < ?", arg);
    }

    /**
     *
     * metoda zwracajaca najnowszy timestamp w tabeli
     *
     */

    public long getNajnowszyTimestamp(){
        long time;
        String x ="SELECT MAX(time) AS xyz FROM kursy";
        Cursor cur = db.rawQuery(x, null);
        cur.moveToFirst();
        time = cur.getLong(0);
        return time;
    }

    /**
     *
     * metoda zwracajaca ilosc rekordow w tabeli
     *
     */

    public long getIloscRekordow(){
        long ilosc;
        Cursor cur = db.rawQuery("SELECT COUNT(*) FROM " + DB_KURSY_TABLE , null);
        cur.moveToFirst();
        ilosc = cur.getLong(0);
        return  ilosc;
    }
}

