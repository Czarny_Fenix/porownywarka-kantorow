package com.figielski.grzegorz.porownywarkakantorow;

import android.app.ActivityManager;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.app.FragmentTransaction;
import android.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.figielski.grzegorz.porownywarkakantorow.bazadanych.KursyDbAdapter;
import com.figielski.grzegorz.porownywarkakantorow.fragmenty.GraphFragment;
import com.figielski.grzegorz.porownywarkakantorow.fragmenty.LocalDataFragment;
import com.figielski.grzegorz.porownywarkakantorow.fragmenty.RemoteDataFragment;
import com.figielski.grzegorz.porownywarkakantorow.fragmenty.TitleFragment;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public ProgressDialog mProgressDialog;
    private int aktywnyFragment;                            //zmienna przechowujaca wartosc fragmentu, ktory aktualnie jest wyswietlany uzytkownikowi
    private String FRAGMENT = "fragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Waluty().execute();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {
            navigationView.getMenu().performIdentifierAction(R.id.nav_strona_tytulowa, 0);
            navigationView.getMenu().getItem(3).setChecked(true);
        } else {
            displayView(savedInstanceState.getInt(FRAGMENT));
        }

    }

    /**
     *
     * metoda uruchamiana w momencie powrotu do activity, jesli uzytkownik zapauzowal activity i wraca do niego
     *
     */

    @Override
    protected void onResume() {
        super.onResume();
        refreshFragment();
        if(!isMyServiceRunning()){
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
            String czas = pref.getString(getApplicationContext().getResources().getString(R.string.sync_frequency), "10");
            int czasx = Integer.parseInt(czas);
            if(czasx != -1){
                Intent intent = new Intent(this,RefreshService.class);
                startService(intent);
            }
        }
    }

    public void setAktywnyFragment(int aktywnyFragment) {
        this.aktywnyFragment = aktywnyFragment;
    }

    public int getAktywnyFragment() {
        return aktywnyFragment;
    }

    public void refreshFragment(){
        displayView(getAktywnyFragment());
    }

    /**
     *
     * @param viewId        - identyfikator fragmentu
     *
     * metoda uruchamiajaca fragment o identyfikatorze viewId
     *
     */

    public void displayView(int viewId){
        Fragment fragment = null;
        String title = getApplicationContext().getResources().getString(R.string.tytul_aplikacji);
        FragmentManager manager;
        manager = getFragmentManager();

        setAktywnyFragment(viewId);

        switch (viewId){
            case R.id.nav_kursy_lokalne:
                fragment = new LocalDataFragment();
                title = getApplicationContext().getResources().getString(R.string.kursy_lokalne_title);
                break;
            case R.id.nav_strona_tytulowa:
                fragment = new TitleFragment();
                title = getApplicationContext().getResources().getString(R.string.strona_tytulowa_title);
                break;
            case R.id.nav_kursy_zdalne:
                fragment = new RemoteDataFragment();
                title = getApplicationContext().getResources().getString(R.string.remote_data_title);
                break;
            case R.id.nav_wykres_kursow:
                fragment = new GraphFragment();
                title = getApplicationContext().getResources().getString(R.string.wykres_kursow_tytul);
                break;
        }

        if (fragment != null) {
            FragmentTransaction ft= manager.beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    /**
     *
     * metoda zamykajaca Drawer
     *
     */

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     *
     * metoda uruchamiana przy kliknieciu w kontekstowe menu
     *
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            startActivity(new Intent(this,SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     *
     * @param item      - element Drawer'a
     *
     * metoda uruchamiana przy kliknieciu na element Drawer'a
     *
     */

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        if(item.isChecked()) item.setChecked(false);
        else item.setChecked(true);

        displayView(item.getItemId());
        return true;
    }

    /**
     *
     * AsyncTask majacy za zadanie aktualizacje kursow bazy lokalnej
     *
     */

    private class Waluty extends AsyncTask<Void, Void, Void>{
        boolean test_polaczenia=false;

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(MainActivity.this);
            mProgressDialog.setTitle(getApplicationContext().getResources().getString(R.string.waluty));
            mProgressDialog.setMessage(getApplicationContext().getResources().getString(R.string.ladowanie));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            int orientacja = getResources().getConfiguration().orientation;
            if(orientacja== Configuration.ORIENTATION_PORTRAIT){
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            } else{
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            test_polaczenia = DatabaseInserter.getInstance(getApplicationContext()).isInternetAvailable();
            if (test_polaczenia) {
                DatabaseInserter.getInstance(getApplicationContext()).aktualizacjaBazy();
            }
            return null;
        }

            @Override
            protected void onPostExecute(Void result){
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

                if(!test_polaczenia){
                    Context context = getApplicationContext();
                    CharSequence text = context.getResources().getString(R.string.blad_polaczenia);
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                } else {
                    KursyDbAdapter kursydb=new KursyDbAdapter(getApplication());
                    kursydb.open();
                    kursydb.deleteStareKursy(getApplicationContext());
                    kursydb.close();
                }
                mProgressDialog.dismiss();
                refreshFragment();
            }

    }

    /**
     *
     * metoda majaca na celu sprawdzenie, czy Service automatycznego odswiezania
     * jest uruchomiony (zeby nie uruchamiac go wielokrotnie)
     *
     */

    private boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (service.service.getClassName().equals(this.getResources().getString(R.string.pelna_nazwa_serwisu))) {
                return true;
            }
        }
        return false;
    }
}
