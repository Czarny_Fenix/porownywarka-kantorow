package com.figielski.grzegorz.porownywarkakantorow;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.SwitchPreference;
import android.support.v7.app.ActionBar;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatDelegate;
import android.view.MenuItem;
import android.support.v4.app.NavUtils;

import java.util.List;

public class SettingsActivity extends PreferenceActivity {

    private AppCompatDelegate mDelegate;

    /**
     *
     * listener wywolywany przy zmianie wartosci ustawien
     *
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);

            } else {
                preference.setSummary(stringValue);
            }
            return true;
        }
    };


    /**
     *
     * metoda przepisujaca nowa wartosc ustawien do wyswietlanej uzytkownikowi wartosci
     *
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
    }

    private void setupActionBar() {
        ActionBar actionBar = getDelegate().getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     *
     * metoda przechwytujaca klikniecie uzytkownika w czesc interfejsu Drawera
     *
     */

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (!super.onMenuItemSelected(featureId, item)) {
                NavUtils.navigateUpFromSameTask(this);
            }
            return true;
        }
        return super.onMenuItemSelected(featureId, item);
    }


    @Override
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.pref_headers, target);
    }

    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || GeneralPreferenceFragment.class.getName().equals(fragmentName)
                || DataSyncPreferenceFragment.class.getName().equals(fragmentName)
                || DataDeletePreferenceFragment.class.getName().equals(fragmentName);
    }

    /**
     *
     * fragment ustawien dotyczacy odswiezania zrodel
     *
     */
    public static class GeneralPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);
            setHasOptionsMenu(true);
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());

            SwitchPreference prefNbp = (SwitchPreference) findPreference(this.getResources().getString(R.string.nbp_switch));
            prefNbp.setSummary(this.getResources().getString(R.string.czas_reakcji) + pref.getString(this.getResources().getString(R.string.nbp_text),"0"));
            SwitchPreference prefWalutomat = (SwitchPreference) findPreference(this.getResources().getString(R.string.walutomat_switch));
            prefWalutomat.setSummary(this.getResources().getString(R.string.czas_reakcji) + pref.getString(this.getResources().getString(R.string.walutomat_text),"0"));
            SwitchPreference prefCinkciarz = (SwitchPreference) findPreference(this.getResources().getString(R.string.cinkciarz_switch));
            prefCinkciarz.setSummary(this.getResources().getString(R.string.czas_reakcji) + pref.getString(this.getResources().getString(R.string.cinkciarz_text),"0"));
            SwitchPreference prefAlior = (SwitchPreference) findPreference(this.getResources().getString(R.string.alior_switch));
            prefAlior.setSummary(this.getResources().getString(R.string.czas_reakcji) + pref.getString(this.getResources().getString(R.string.alior_text),"0"));
            SwitchPreference prefInternetowy = (SwitchPreference) findPreference(this.getResources().getString(R.string.internetowy_switch));
            prefInternetowy.setSummary(this.getResources().getString(R.string.czas_reakcji) + pref.getString(this.getResources().getString(R.string.internetowy_text),"0"));
            SwitchPreference prefEkspert = (SwitchPreference) findPreference(this.getResources().getString(R.string.ekspert_switch));
            prefEkspert.setSummary(this.getResources().getString(R.string.czas_reakcji) + pref.getString(this.getResources().getString(R.string.ekspert_text),"0"));
            EditTextPreference prefRekordy = (EditTextPreference) findPreference(this.getResources().getString(R.string.ilosc_rekordow_text));
            prefRekordy.setTitle(this.getResources().getString(R.string.rekordy_w_bazie) + pref.getString(this.getResources().getString(R.string.ilosc_rekordow_text), "0"));
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }

    /**
     *
     * fragment ustawien dotyczacy automatycznego pobierania nowych kursow
     *
     */
    public static class DataSyncPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_data_sync);
            setHasOptionsMenu(true);

            bindPreferenceSummaryToValue(findPreference(this.getResources().getString(R.string.sync_frequency)));
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }


    /**
     *
     * fragment ustawien odpowiedzialny za czyszczenie wewnetrznej bazy danych z starych kursow
     *
     */
    public static class DataDeletePreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_data_delete);
            setHasOptionsMenu(true);

            bindPreferenceSummaryToValue(findPreference(this.getResources().getString(R.string.delete_frequency)));
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }

    private AppCompatDelegate getDelegate() {
        if (mDelegate == null) {
            mDelegate = AppCompatDelegate.create(this, null);
        }
        return mDelegate;
    }
}
