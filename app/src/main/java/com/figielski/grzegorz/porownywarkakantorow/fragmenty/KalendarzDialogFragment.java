package com.figielski.grzegorz.porownywarkakantorow.fragmenty;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by Grzesiek on 23.05.2017.
 */

public class KalendarzDialogFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar kalendarz = Calendar.getInstance();
        int year = kalendarz.get(Calendar.YEAR);
        int month = kalendarz.get(Calendar.MONTH);
        int day = kalendarz.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    /**
     *
     * metoda uruchamiana, gdy uzytkownik wybierze date z kalendarza
     *
     */

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        RemoteDataFragment fragmentMatka = (RemoteDataFragment) getTargetFragment();
        fragmentMatka.ustawDate(year, month+1, dayOfMonth);
    }

}
