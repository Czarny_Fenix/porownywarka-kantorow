package com.figielski.grzegorz.porownywarkakantorow;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Random;

/**
 * Created by Grzesiek on 17.05.2017.
 */

public class RefreshService extends IntentService {

    public RefreshService() {
        super("Refreshing_Waluty");
    }


    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     *
     * metoda dzialajaca w tle, dopoki nie skonczy dzialania, dopoty service jest aktywny, po skonczeniu dzialania
     * uruchamiana jest metoda onDestroy
     *
     * metoda ta konczy dzialanie tylko w przypadku, kiedy w SharedPreferences znajdzie sie wartosc mowiaca,
     * aby aplikacja nie dokonywala automatycznej aktualizacji kursow
     *
     */

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        synchronized (this){
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
            String tmp;                                                                    //zmienna przechowujaca ustawienia czasu w postaci stringa
            int czas;
            Random r = new Random();
            int los;

            tmp = pref.getString(this.getResources().getString(R.string.sync_frequency), "10");
            czas = Integer.parseInt(tmp);

            while(czas!=-1){
                try{
                    los = r.nextInt(12000 - 6000) + 6000;
                    wait((1000 * 60 * czas) + los);

                    tmp = pref.getString(this.getResources().getString(R.string.sync_frequency), "10");
                    czas = Integer.parseInt(tmp);

                    if(czas==-1){
                        break;
                    }
                        DatabaseInserter.getInstance(this).aktualizacjaBazy();
                } catch(Exception e){
                    Log.e(this.getResources().getString(R.string.zlapano_exception), this.getResources().getString(R.string.refreshservice));
                }

                tmp = pref.getString(this.getResources().getString(R.string.sync_frequency), "10");
                czas = Integer.parseInt(tmp);
            }
        }
    }
}
